import { Link } from 'react-router-dom';


export default function AttendeesList (props) {
    return (
        <>
            <table className="table table-striped">
                <thead>
                    <tr>
                        <th>Name</th>
                        <th>Conference</th>
                    </tr>
                </thead>
                <tbody>
                    {props.attendees.map(attendee => {
                    return (
                        <tr key={attendee.href} >
                            <td>{ attendee.name }</td>
                            <td>{ attendee.conference }</td>
                        </tr>
                    );
                    })}
                </tbody>
            </table>
            <div className="d-grid gap-2 d-sm-flex justify-content-sm-center">
            <Link to="/attendees/new" className="btn btn-primary btn-lg px-4 gap-3">Attend a conference</Link>
            </div>
        </>
        
    );
}