
function deletePlaceholder() {
    let placeholders = document.querySelectorAll('.placeholder');
    placeholders.forEach(placeholder => {
        placeholder.classList.remove('placeholder');
    })
    let glow = document.querySelectorAll('.placeholder-glow');
    glow.forEach(glow => {
        glow.classList.remove('placeholder-glow')
    })
}

function createCard(name, description, pictureUrl, starts, end, location) {
    return `
        <div class="col">
            <div class="card shadow p-0 m-1 bg-body-tertiary rounded">
                <img src="${pictureUrl}" class="card-img-top">
                
                <div class="card-body">
                    <h5 class="card-title">${name}</h5>
                    <div class="card-subtitle mt-1 mb-1 text-muted">
                        ${location}
                    </div>
                    <p class="card-text">${description}</p>
                </div>
                <div class="list-group-item">
                    ${starts} - ${end}
                </div>
            </div>
        </div>     
    `;
}

// function createCard(name, description, pictureUrl, starts, end, location) {
//     return `
//         <div class="col">
//             <div class="card shadow p-0 m-1 bg-body-tertiary rounded placeholder-glow">
//                 <img src="${pictureUrl}" class="card-img-top placeholder">
                
//                 <div class="card-body">
//                     <h5 class="card-title placeholder">${name}</h5>
//                     <div class="card-subtitle mt-1 mb-1 text-muted placeholder">
//                         ${location}
//                     </div>
//                     <p class="card-text placeholder">${description}</p>
//                 </div>
//                 <div class="list-group-item placeholder">
//                     ${starts} - ${end}
//                 </div>
//             </div>
//         </div>     
//     `;
// }

function createPlaceholder() {
    return `
        <div class="col" id="place">
            <div class="card placeholder-glow">
                <img src="" class="card-img-top placeholder" alt="...">
                <div class="card-body">
                    <h5 class="card-title placeholder-glow">
                        <span class="placeholder col-6"></span>
                    </h5>
                    <p class="card-text placeholder-glow">
                        <span class="placeholder col-7"></span>
                        <span class="placeholder col-4"></span>
                        <span class="placeholder col-4"></span>
                        <span class="placeholder col-6"></span>
                        <span class="placeholder col-8"></span>
                    </p>
                </div>
            </div>
        </div>  
    `;
}

function removePlaceholder() {
    let placeholder = document.getElementById('place');
    placeholder.remove();
}

window.addEventListener('DOMContentLoaded', async () => {
    console.log("DOM content loaded")
    const url = 'http://localhost:8000/api/conferences/';
    try {
        const response = await fetch(url);
        if (!response.ok) {
            
            throw new Error('Response not ok');
        } else {
            console.log(response, 'Response');

            const data = await response.json();
            console.log(data, 'Data');

            const conference = data.conferences[0];
            console.log(conference, 'Conference');

            // const nameTag = document.querySelector('.card-title');
            // nameTag.innerHTML = conference.name;
            
            // for (let i = 0; i < data.conferences.length; i++) {
            //     const row = document.querySelector('.row');
            //     const placeholder = createPlaceholder();
            //     row.innerHTML += placeholder;
            // }

            for (let conference of data.conferences) {
                // let conference = data.conferences
                const detailUrl = `http://localhost:8000${conference.href}`;
                const detailResponse = await fetch(detailUrl);
                const placeholder = createPlaceholder();
                const row = document.querySelector('.row');
                row.innerHTML += placeholder;

                if (detailResponse.ok) {
                    
                    const details = await detailResponse.json();
                    console.log(details, "Details")
                    const title = details.conference.name;

                    const description = details.conference.description;

                    const pictureUrl = details.conference.location.picture_url;

                    const starts = new Date(details.conference.starts).toDateString();

                    const ends = new Date(details.conference.ends).toDateString();

                    const location = details.conference.location.name;

                    const html = createCard(title, description, pictureUrl, starts, ends, location);
                    
                    setTimeout(() => {
                        removePlaceholder();
                        // deletePlaceholder();
                        row.innerHTML += html;
                    }, 500);
                    
                    // const imageTag = document.querySelector('.card-img-top');
                    // imageTag.src = details.conference.location.picture_url;
                }
            }

            

        }
    } catch (e) {
        const alert = document.querySelector('.container');
        alert.innerHTML = `<div role="alert" class="alert alert-dismissible">Error, something failed here</div>`
        console.error('error', error);
    }
});
