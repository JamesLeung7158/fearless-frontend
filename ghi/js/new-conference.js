window.addEventListener('DOMContentLoaded', async () => {
    console.log("DOM content loaded");
    const url = 'http://localhost:8000/api/locations/';
    try {
        const response = await fetch(url);
        if (response.ok) {
            console.log(response, 'Response');
            const data = await response.json();
            console.log(data, 'Data')

            const selectTag = document.getElementById('location');
            for (let location of data.locations) {
                const option = document.createElement('option');
                option.value = location.id;
                option.innerHTML = location.name;
                selectTag.appendChild(option);
            }
        }
    } catch (e) {
        console.error('error', error);
    }

    const formTag = document.getElementById('create-conference-form');
    formTag.addEventListener('submit', async event => {
        event.preventDefault();
        const formData = new FormData(formTag);
        const json = JSON.stringify(Object.fromEntries(formData));

        const conferenceUrl = 'http://localhost:8000/api/conferences/';
        const fetchConfig = {
            method: "post",
            body: json,
            headers: {
                'Content-type': 'application/json'
            },
        };

        const response = await fetch(conferenceUrl, fetchConfig);
        if (response.ok) {
            formTag.reset();
            const newConference = await response.json();
            console.log(newConference);
        }
        
    })
});