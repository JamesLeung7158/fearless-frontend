window.addEventListener('DOMContentLoaded', async () => {
    console.log("DOM content loaded");
    const url = 'http://localhost:8000/api/states/';
    try {
        const response = await fetch(url);
        if (response.ok) {
            console.log(response, 'Response');
            const data = await response.json();
            console.log(data, 'Data')

            const selectTag = document.getElementById('state');
            for (let state of data.states) {
                const option = document.createElement('option');
                option.value = state.abbreviation;
                option.innerHTML = state.name;
                selectTag.appendChild(option);
            }
        }
    } catch (e) {
        console.error('error', error);
    }

    const formTag = document.getElementById('create-location-form');
    formTag.addEventListener('submit', async event => {
        event.preventDefault();
        const formData = new FormData(formTag);
        const json = JSON.stringify(Object.fromEntries(formData));

        const locationUrl = 'http://localhost:8000/api/locations/';
        const fetchConfig = {
            method: "post",
            body: json,
            headers: {
                'Content-type': 'application/json'
            },
        };

        const response = await fetch(locationUrl, fetchConfig);
        if (response.ok) {
            formTag.reset();
            const newLocation = await response.json();
            console.log(newLocation);
        }
        
        
    })
});