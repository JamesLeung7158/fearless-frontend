// Get the cookie out of the cookie store
const payloadCookie = await cookieStore.get('jwt_access_payload');
if (payloadCookie) {
    // The cookie value is a JSON-formatted string, so parse it
    const encodedPayload = payloadCookie.value;

    // Convert the encoded payload from base64 to normal string
    const decodedPayload = atob(encodedPayload);

    // The payload is a JSON-formatted string, so parse it
    const payload = JSON.parse(decodedPayload);

    // Print the payload
    console.log(payload, 'payload');

    if (payload.user.perms.includes("events.add_conference")) {
        document.getElementById('conferences').classList.remove('d-none');
    }

    if (payload.user.perms.includes("events.add_location")) {
        document.getElementById('locations').classList.remove('d-none');
    }

    if (payload.user.perms.includes("presentations.add_presentation")) {
        document.getElementById('presentations').classList.remove('d-none');
    }

}







