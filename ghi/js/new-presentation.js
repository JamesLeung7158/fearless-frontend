window.addEventListener('DOMContentLoaded', async () => {
    console.log("DOM content loaded");
    const url = 'http://localhost:8000/api/conferences/';
    try {
        const response = await fetch(url);
        if (response.ok) {
            console.log(response, 'Response');
            const data = await response.json();
            console.log(data, 'Data')

            const selectTag = document.getElementById('conference');
            for (let conference of data.conferences) {
                const option = document.createElement('option');
                // option.value = conference.id;
                const href = conference.href;
                const hrefsplit = href.split("/");
                const id = hrefsplit[hrefsplit.length - 2];
                console.log(id);
                option.value = id;
                option.innerHTML = conference.name;
                selectTag.appendChild(option);
            }
        }
    } catch (e) {
        console.error('error', error);
    }

    const formTag = document.getElementById('create-presentation-form');
    formTag.addEventListener('submit', async event => {
        event.preventDefault();
        const formData = new FormData(formTag);
        const json = JSON.stringify(Object.fromEntries(formData));

        const select = document.getElementById('conference')
        const presentationUrl = `http://localhost:8000/api/conferences/${select.options[select.selectedIndex].value}/presentations/`;
        const fetchConfig = {
            method: "post",
            body: json,
            headers: {
                'Content-type': 'application/json'
            },
        };

        const response = await fetch(presentationUrl, fetchConfig);
        if (response.ok) {
            formTag.reset();
            const newPresentation = await response.json();
            console.log(newPresentation);
        }
        
    })
});